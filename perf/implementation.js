var pull = require('pull-stream');
var Pausable = require('pull-pause');
var usage = require('cpu-percentage');

function drainGently(a, b, c) {
  var opFirst = !a || typeof a === 'function';
  var opts = opFirst ? {ceiling: 88, wait: 144} : a;
  var op = opFirst ? a : b;
  var opDone = opFirst ? b : c;
  const ceiling = opts.ceiling;
  const wait = opts.wait;
  const pausable = Pausable();
  let stats = usage();
  const usageAtStart = stats;
  let step = 2;
  let count = 0;

  function checkpoint() {
    stats = usage(usageAtStart);
    process.stdout.write(`${stats.time},${stats.percent}\n`);
    if (stats.percent < ceiling || stats.time < 20) {
      step = step << 1 || 1;
      pausable.resume();
    } else {
      step = step >> 1 || 1;
      pausable.pause();
      setTimeout(checkpoint, wait);
    }
  }

  function opWrapper(x) {
    var res;
    if (op) res = op(x);
    if (++count >= step) {
      count = 0;
      checkpoint();
    }
    return res;
  }

  return pull(pausable, pull.drain(opWrapper, opDone));
}

module.exports = drainGently;
